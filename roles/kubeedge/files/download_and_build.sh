#!/bin/sh
export PATH=$PATH:/usr/local/go/bin
eval $(/usr/local/go/bin/go env)
git clone https://github.com/kubeedge/kubeedge.git \
  "${GOPATH}/src/github.com/kubeedge/kubeedge"
cd "${GOPATH}/src/github.com/kubeedge/kubeedge"
"${GOPATH}/src/github.com/kubeedge/kubeedge/build/tools/certgen.sh" genCertAndKey edge
cd ${GOPATH}/src/github.com/kubeedge/kubeedge/
make all WHAT=cloudcore
